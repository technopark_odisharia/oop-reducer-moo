package Local::Source::FileHandler;

use Moo;
use Types::Standard qw/ FileHandle /;
use Tie::File;

extends 'Local::Source';

has fh  => ( is => 'ro', isa => FileHandle );

sub _build_iterable {
    my $self = shift;
    my @array;
    tie @array, 'Tie::File', $self->fh;
    return \@array;
};

1;
