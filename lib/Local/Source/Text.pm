package Local::Source::Text;

use Moo;
use Types::Standard qw/ Str ScalarRef /;

extends 'Local::Source';

has text       => ( is => 'ro', isa => Str );
has delimiter  => ( is => 'ro', isa => Str, default => "\n" );

sub _build_iterable {
    my $self = shift;
    my $ar = [ split(/\Q${\( $self->delimiter )}\E/, $self->text) ];
    return $ar;
};

1;
