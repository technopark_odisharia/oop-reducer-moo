package Local::Source::Array;

use Moo;
use Types::Standard qw/ ArrayRef /;

extends 'Local::Source';

has array   => ( is => 'ro', isa => ArrayRef, requred => 1 );

sub _build_iterable {
    my $self = shift;
    return $self->array;
};

1;
