package Local::Row::Simple;

use Moo;
use Types::Standard qw/ HashRef /;
use DDP;

extends 'Local::Row';

has string   => ( is => 'ro', isa => HashRef, required => 1 );

around new => sub {
    my ( $orig, $class, %args ) = @_;
    my %hash;

    if ( $args{str} eq "" ) {
        $class->$orig({ string => {} });
        return {};
    } else {
        foreach my $string ( split /,/, $args{str} ) {
            my @pair = split(/:/, $string);
            $hash{$pair[0]} = $pair[1];
            return undef unless ( $pair[1] and $pair[0] and scalar @pair == 2 );
        }
    }
    $class->$orig({ string => \%hash });
    # return \%hash;
};

sub get {
    my ( $self, $name, $default_val ) = @_;

    my %hash = %{$self->string};

    if ( defined $hash{$name} ) {
        return $hash{$name};
    } else {
        return $default_val
    }
};

1;
