package Local::Row::JSON;

use Moo;
use JSON::XS;

use DDP;

extends 'Local::Row';

has _json    => ( is => 'ro', reader => 'json' );

around new => sub {
    my ( $orig, $class, %args ) = @_;
    if ( $args{str} eq '{}' ) {
        # $class->$orig({ _json => '{}' });
        return {};
    }
    my $json = eval { decode_json($args{str}) } or return undef;
    return undef unless ref($json) eq 'HASH';
    $class->$orig({ _json => $json });
    # return $json;
};

sub get {
    my ( $self, $name, $default ) = @_;
    my @field = $self->get_fields($name);
    if ( scalar @field ) {
        if ( scalar @field == 1 ) {
            return $field[0]
        } else {
            return @field
        }
    } else {
        return $default
    }
};

sub get_fields {
    my ( $self, $name ) = @_;
    my @field;

    my $JSON_search_field;
    $JSON_search_field = sub {
        my $json = shift;
        if ( ref($json) eq 'HASH' ) {
            foreach ( keys %{$json} ) {
                if ( ref $json->{$_} eq 'HASH' ) {
                    $JSON_search_field->($json->{$_});
                } else {
                    push(@field, $json->{$name}) if ( $json->{$name} );
                }
            }
        } elsif ( ref $json eq 'ARRAY' ) {
            foreach ( @{$json} ) {
                ( ref($_) eq 'HASH' )
                ? ( $JSON_search_field->($_) )
                : ( next )
            }
        }
    };
    $JSON_search_field->($self->json);

    return @field;
};

1;
