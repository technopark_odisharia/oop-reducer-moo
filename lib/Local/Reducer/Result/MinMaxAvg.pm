package Local::Reducer::Result::MinMaxAvg;

use Moo;
use Types::Standard qw/ Str Num /;

has min     => ( is => 'rwp', isa => Str, reader => 'get_min', required => 0 );
has max     => ( is => 'rwp', isa => Str, reader => 'get_max', required => 0 );
has avg     => ( is => 'rwp', isa => Str, reader => 'get_avg', required => 0 );

around BUILDARGS => sub {
  my ( $orig, $class, %args ) = @_;
  return $class->$orig({
      min => $args{init},
      max => $args{init},
      avg => $args{init},
  });
};

sub update {
    my ( $self, %hash ) = @_;
    $self->_set_min($hash{min}) if $hash{min};
    $self->_set_max($hash{max}) if $hash{max};
    $self->_set_avg($hash{avg}) if $hash{avg};
};

1;
