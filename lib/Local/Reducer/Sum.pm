package Local::Reducer::Sum;

use Moo;
use Types::Standard qw/ Str /;
use Scalar::Util qw/ looks_like_number /;

use DDP;

extends 'Local::Reducer';

has field   => ( is => 'ro', isa => Str, required => 1 );

sub internal_reduce {
    my ( $self, $row, $value ) = @_;
    my $add = $row->get($self->field, 0) if defined $row;
    if (defined $row and (looks_like_number($add))) {
            return $value += $add;
    };
    return $value;
};

1;
