package Local::Reducer::MinMaxAvg;

use Moo;
use Types::Standard qw/ Str Num /;

require Local::Reducer::Result::MinMaxAvg;

extends 'Local::Reducer';

has field       => ( is => 'ro', isa => Str, required => 1 );
has counter     => ( is => 'rwp', isa => Num, default => 0 );

around _build_reduced => sub {
    my ( $orig, $self, %args ) = @_;
    return Local::Reducer::Result::MinMaxAvg->new(init => $self->initial_value);
};

before reduce_all => sub {
    my $self = shift;
    $self->_set_counter(0);
};

before _reset_reduced => sub {
    my $self = $_[0];
    $_[1] = Local::Reducer::Result::MinMaxAvg->new(init => $self->initial_value);
};

sub internal_reduce {
    my ( $self, $row, $value ) = @_;
    return $value unless defined $row;

    my $field_value = $row->get($self->field, 0);
    $self->_set_counter($self->counter + 1);

    my $min = $value->get_min > $field_value || $self->counter == 1 ? $field_value : undef;
    my $max = $value->get_max < $field_value ? $field_value : undef;
    my $avg = ( $value->get_avg * ($self->counter - 1) + $field_value ) / $self->counter;
    $value->update(
        min => $min,
        max => $max,
        avg => $avg,
    );
    return $value;
};

1;
