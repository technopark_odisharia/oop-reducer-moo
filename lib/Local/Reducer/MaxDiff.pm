package Local::Reducer::MaxDiff;

use Moo;
use Types::Standard qw/ Str Num /;

extends 'Local::Reducer';

has top     => ( is => 'ro', isa => Str, required => 1 );
has bottom  => ( is => 'ro', isa => Str, required => 1 );

sub internal_reduce {
    my ( $self, $row, $value ) = (@_, 0);
    return $value unless defined $row;

    my ( $top, $bottom ) = (
        $row->get($self->top, 0),
        $row->get($self->bottom, 0)
    );

    $value = abs($top - $bottom) if $value < abs($top - $bottom);
    return $value;
};

1;
