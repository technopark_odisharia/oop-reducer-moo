package Local::Reducer;

use Moo;
use Types::Standard qw/ InstanceOf Num Str /;

=encoding utf8

=head1 NAME

Local::Reducer - base abstract reducer

=head1 VERSION

Version 1.00

=cut

our $VERSION = '1.00';

=head1 SYNOPSIS

=cut

has source          => ( is => 'ro', isa => InstanceOf["Local::Source"] );
has row_class       => ( is => 'ro', isa => Str );
has initial_value   => ( is => 'ro', isa => Num, default => 0 );
has reduced         => ( is => 'rw', builder => '_build_reduced' );

sub _build_reduced {
    my $self = shift;
    return $self->initial_value;
}

sub reduce_n {
    my ( $self, $n ) = @_;
    while ( $n-- and my $str = $self->source->next ) {
        $self->reduced($self->internal_reduce( $self->row_class->new(str => $str), $self->reduced ));
    };
    return $self->reduced;
}

sub reduce_all {
    my $self = shift;
    $self->source->reset_iterator;
    $self->_reset_reduced($self->initial_value);

    while ( my $str = $self->source->next ) {
        $self->reduced($self->internal_reduce( $self->row_class->new(str => $str), $self->reduced));
    };
    return $self->reduced;
}

sub _reset_reduced {
    my ( $self, $value ) = @_;
    $self->reduced($value);
}

1;
