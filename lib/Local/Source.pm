package Local::Source;

use Moo;
use Types::Standard qw/ Int ArrayRef /;

has iterable       => ( is => 'rw', isa => ArrayRef, lazy => 1, builder => '_build_iterable' );
has iterator       => ( is => 'rw', isa => Int, default => 0 );

sub next {
    my $self = shift;
    if ( $self->iterable->[$self->iterator] ) {
        my $return_iterator = $self->iterator;
        $self->iterator($self->iterator + 1);
        return $self->iterable->[$return_iterator]
    } else {
        return undef;
    }
}

sub reset_iterator {
    my $self = shift;
    $self->iterator(0);
}

1;
